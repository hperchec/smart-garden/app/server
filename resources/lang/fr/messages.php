<?php

// resources/lang/fr/messages.php

return [
    'mail' => [
        'hello' => 'Bonjour,',
        'regards' => 'Cordialement',
        'verifyEmail' => [
            'subject' => 'Confirmation de votre adresse email',
            'line1' => 'Cliquez sur le bouton ci-dessous pour confirmer votre adresse.',
            'button' => 'Valider mon adresse',
            'line2' => 'Si vous n\'avez pas créer de compte, veuillez ignorer ce message.'
        ]
    ]
];