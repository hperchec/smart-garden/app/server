<?php

// resources/lang/en/messages.php

return [
    'mail' => [
        'hello' => 'Hello,',
        'regards' => 'Regards',
        'verifyEmail' => [
            'subject' => 'Welcome to our application!',
            'line1' => 'Please click the button below to verify your email address.',
            'button' => 'Verify Email Address',
            'line2' => 'If you did not create an account, no further action is required.'
        ]
    ]
];