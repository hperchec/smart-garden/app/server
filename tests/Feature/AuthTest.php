<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * Set up method
     */
    public function setUp() :void
    {
        // Always call parent set up
        parent::setUp();
    }

    /**
     * Test login route
     * 
     * @author Hervé Perchec
     * @group Authentication
     * @covers App\Http\Controllers\AuthController::login
     * @covers App\Http\Requests\Auth\LoginRequest
     * 
     * @return void
     */
    public function test_login ()
    {
        // Valid request
        $response = $this->postJson('/api/login', [
            'username' => 'admin',
            'password' => 'admin'
        ]);
        // Get access_token
        $token = json_decode($response->content())->access_token;
        // Passed
        $response->assertStatus(200);
        // Return token for dependency
        return $token;
    }

    /**
     * Test 'me' route
     * 
     * @author Hervé Perchec
     * @group Authentication
     * @covers App\Http\Controllers\AuthController::getCurrentUser
     * @covers App\Policies\AuthPolicy::getCurrentUser
     * @covers App\Http\Requests\Auth\GetCurrentUserRequest
     * 
     * @depends test_login
     * 
     * @param string $token - User access token
     * @return void
     */
    public function test_get_user_data (string $token)
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->get('/api/me');
        $response->assertStatus(200)
                 ->assertJson([
                     'username' => 'admin'
                 ]);
    }

    /**
     * Test update user password
     * 
     * @author Hervé Perchec
     * @group Auth
     * @covers App\Http\Controllers\AuthController::updatePassword
     * @covers App\Policies\AuthPolicy::updatePassword
     * @covers App\Http\Requests\Auth\UpdatePasswordRequest
     * 
     * @depends test_login
     * 
     * @param string $token - User access token
     * @return void
     */
    public function test_update_user_password (string $token)
    {
        $response = $this->withHeaders([
                        'Authorization' => 'Bearer ' . $token,
                    ])
                    ->patch('/api/me/password', [
                        'current_password' => 'test',
                        'password' => 'Upd4t3dP4$$w0rd123*',
                        'password_confirmation' => 'Upd4t3dP4$$w0rd123*'
                    ]);
        $response->assertStatus(200);
    }

    /**
     * Test logout route
     * 
     * @author Hervé Perchec
     * @group Authentication
     * @covers App\Http\Controllers\AuthController::logout
     * @covers App\Policies\AuthPolicy::logout
     * @covers App\Http\Requests\Auth\LogoutRequest
     * 
     * @depends test_login
     * 
     * @param string $token - User access token
     * @return void
     */
    public function test_logout (string $token)
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->post('/api/logout');
        $response->assertStatus(200);
    }

}
