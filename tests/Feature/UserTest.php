<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Laravel\Passport\Passport;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

use App\Models\User;

class UserTest extends TestCase
{
    /**
     * Set up method
     */
    public function setUp() :void
    {
        // Always call parent set up
        parent::setUp();
        // Do logic here
    }

    /**
     * Test create user
     * 
     * @author Hervé Perchec
     * @group User
     * @covers App\Http\Controllers\UserController::create
     * @covers App\Http\Requests\User\CreateRequest
     * 
     * @return User
     */
    public function test_create_user ()
    {
        // Valid request
        $response = $this->post('/api/users/create', [
            'password' => 'Azerty123*',
            'password_confirmation' => 'Azerty123*',
            'username' => 'test'
        ]);
        $response->assertStatus(201);
        // Get created user ID
        $userId = json_decode($response->content())->id;
        // Return created user
        return User::findOrFail($userId);
    }

    /**
     * Test update user
     * 
     * @author Hervé Perchec
     * @group User
     * @covers App\Http\Controllers\UserController::update
     * @covers App\Policies\UserPolicy::update
     * @covers App\Http\Requests\User\UpdateRequest
     * 
     * @depends test_create_user
     * 
     * @param User $user - The previously created user
     * @return void
     */
    public function test_update_user (User $user)
    {
        // Authenticate as user
        Passport::actingAs($user);
        // Valid request
        $response = $this->patch('/api/users/' . $user->id, [
            'password' => 'Azerty123*'
        ]);
        $response->assertStatus(200);
    }

}
