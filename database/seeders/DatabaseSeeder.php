<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Passport seeders
        $this->call(OAuthClientsTableSeeder::class);
        $this->call(OAuthPersonalAccessClientsTableSeeder::class);
        // Errors
        $this->call(ErrorsTableSeeder::class);
        // Users
        $this->call(UsersTableSeeder::class);
    }
}
