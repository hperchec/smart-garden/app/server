<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OAuthClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_clients')->insert([
            [
                'id' => 1,
                'name' => "App Personal Access Client",
                'secret' => "Da20ZKKMS8p1s1cqPR3H4Cys1OIoUqpH4Ue6j3W6",
                'provider' => null,
                'redirect' => "http://localhost",
                'personal_access_client' => "1",
                'password_client' => "0",
                'revoked' => "0",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
            [
                'id' => 2,
                'name' => "App Password Grant Client",
                'secret' => "m4WUbg5qZDUwhCvTRRTn5iNTW1rOlhPSKYIoJfgB",
                'provider' => 'users',
                'redirect' => "http://localhost",
                'personal_access_client' => "0",
                'password_client' => "1",
                'revoked' => "0",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        ]);
    }
}