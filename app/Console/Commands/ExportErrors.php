<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Illuminate\Support\Facades\Storage;

use App\Models\Error;

class ExportErrors extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:errors';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export translated errors from database in JSON format.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Get all errors from database
        $errors = Error::all();
        // Prepare each translated array
        $en_errors = [ 'errors' => [] ];
        $fr_errors = [ 'errors' => [] ];
        // Map
        foreach ($errors as $error) {
            $en_errors['errors'][$error['error_id']] = $error['en_message'];
            $fr_errors['errors'][$error['error_id']] = $error['fr_message'];
        }
        // Put contents for EN translations
        Storage::disk('errors')->put('en.json', json_encode($en_errors, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
        // Put contents for FR translations
        Storage::disk('errors')->put('fr.json', json_encode($fr_errors, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
        // Print message in output
        $this->line('<fg=green>Errors successfully exported!</>');
        $this->line('<fg=cyan;bg=white> EN </>: <fg=cyan>/storage/app/errors/en.json</>');
        $this->line('<fg=cyan;bg=white> FR </>: <fg=cyan>/storage/app/errors/fr.json</>');
        
    }
}
