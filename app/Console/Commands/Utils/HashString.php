<?php

namespace App\Console\Commands\Utils;

use Illuminate\Console\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Illuminate\Support\Facades\Hash;

class HashString extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'utils:hash {string}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Hash the given string.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Hash string
        $hash = Hash::make($this->argument('string'));
        // Print message in output
        $this->line('<fg=green>Result:</> <fg=cyan>' . $hash . '</>');
    }
}
