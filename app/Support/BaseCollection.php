<?php

namespace App\Support;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class BaseCollection extends Collection {

    /**
     * Return array of Model instances calling withoutRelations() method for each item.
     * SEE ALSO -> App\Models\BaseModel withoutRelations() method
     * @return array
     */
    public function withoutRelations()
    {
        $tmp = $this->map(function($item, $key) {
            return $item->withoutRelations();
        });
        return $tmp;
    }
}