<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

/**
 * @codeCoverageIgnore
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Register global functions
        require_once base_path().'/app/GlobalFunctions.php';
        // Authenticated user shortcut
        $this->app->singleton('auth_user', function () {
            return Auth::user();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Send email always to same testing address in development mode
        if ($this->app->environment('local')) {
            Mail::alwaysTo(env('MAIL_ALWAYS_TO'));
        }
    }
}
