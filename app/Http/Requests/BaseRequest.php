<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

use App\Models\Error;

class BaseRequest extends FormRequest
{

    /**
     * Create a new BaseRequest instance
     * @return void
     */
    public function __construct()
    {
        $this->failedAuthorizationResponse = errorResponse(403, Error::find('e0012'));
    }

    /**
     * The response for failed authorization
     * @var @return Illuminate\Http\Response
     */
    protected $failedAuthorizationResponse = null; // Defined in construct method

    /**
     * Handle a failed validation attempt
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        // SEE ALSO -> App\Services\Validator

        // Format errors messages
        $validatorErrors = $validator->errors()->messages();
        // Format from:
        // <field>: [ '<rule1_error_message>', 'rule2_error_message', ... ]
        // to:
        // <field>: { 'rule:<rule1_name>': '<rule1_error_message>', 'rule:<rule2_name>': 'rule2_error_message', ... }
        $formattedErrors = [];
        foreach ($validatorErrors as $fieldName => $errors) {
            $formattedErrors[$fieldName] = [];
            // Loop on errors
            foreach ($errors as $rule => $error) {
                $rule = array_keys($error)[0];
                $formattedErrors[$fieldName][$rule] = $error[$rule];
            }
        }

        // Prepare response
        $response = errorResponse(422, Error::find('e0050'), [
            'fields' => $formattedErrors
        ]);

        throw new ValidationException($validator, $response);
    }

    /**
     * Handle a failed authorization attempt
     * @return void
     * @throws Illuminate\Http\Exceptions\HttpResponseException
     */
    protected function failedAuthorization()
    {
        throw new HttpResponseException($this->failedAuthorizationResponse);
    }
}
