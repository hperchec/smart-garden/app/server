<?php

namespace App\Http\Requests\Circuit;

use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

use App\Http\Requests\BaseRequest;

/**
 * @OA\Schema(schema="Requests.Circuit.Turn"){
 *     required={
 *         "status"
 *     }
 * }
 */
class TurnRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request
     * @return bool
     */
    public function authorize()
    {
        // Authorize -> App\Policies\CircuitPolicy::turn
        Gate::authorize('turn', $this->route('circuit'));
        return true;
    }

    /**
     * @OA\Property(
     *     property="status",
     *     type="boolean",
     *     description="Statut: true or false"
     * )
     *
     * ----
     * 
     * Get the validation rules that apply to the request
     * @return array
     */
    public function rules()
    {
        return [
            'status' => [
                'required',
                'boolean'
            ]
        ];
    }

    /**
     * Get the error messages for the defined validation rules
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
