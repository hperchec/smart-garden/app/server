<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Illuminate\Auth\Events\PasswordReset;

use App\Models\Error;
use App\Models\User;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\LogoutRequest;
use App\Http\Requests\Auth\UpdatePasswordRequest;
use App\Http\Requests\Auth\GetCurrentUserRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;

class AuthController extends Controller
{
    /**
     * Create a new controller instance
     * @codeCoverageIgnore
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @OA\Post(
     *     path="/login",
     *     operationId="auth.login",
     *     summary="Login user",
     *     description="**Connecter l'utilisateur à l'aide de ses identifiants**",
     *     tags={"Auth"},
     *     @OA\RequestBody(
     *         description="User credentials to authenticate",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Requests.Auth.Login")
     *         )
     *     ),
     *     @OA\Response(response=200, description="Successful operation", @OA\MediaType(mediaType="application/json")),
     *     @OA\Response(response=401, description="Unauthorized (bad credentials)"),
     *     @OA\Response(response=422, description="Unprocessable entity")
     * )
     *
     * ----
     * 
     * Login
     * @param LoginRequest $request - The request
     * @return Response
     */
    public function login (LoginRequest $request)
    {
        // Validate the request
        $validated = $request->validated();

        // Check credentials
        if (!auth()->attempt($validated)) {
            return errorResponse(401, Error::find('e0020'));
        }

        // Create access token
        $createToken = auth()->user()->createToken('API Token');
        $accessToken = $createToken->accessToken;
        // $refreshToken = $createToken->refreshToken;

        // Return token
        return response([ 
            'token_type' => 'Bearer',
            'access_token' => $accessToken
            // 'refresh_token' => $refreshToken
        ]);
    }

    /**
     * @OA\Post(
     *     path="/logout",
     *     operationId="auth.logout",
     *     summary="Logout authenticated user",
     *     description="**Déconnecter l'utilisateur authentifié**  
               ❗ Action limitée au compte utilisateur authentifié par token
           ",
     *     tags={"Auth"},
     *     @OA\RequestBody(
     *         description="Request for logout action",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Requests.Auth.Logout")
     *         )
     *     ),
     *     @OA\Response(response=200, description="Successful operation", @OA\MediaType(mediaType="application/json")),
     *     @OA\Response(response=401, description="Unauthenticated")
     * )
     *
     * ----
     * 
     * Logout authenticated user
     * @param LogoutRequest $request - The request
     * @return Response
     */
    public function logout (LogoutRequest $request)
    {
        // Revoke authenticated user token
        app('auth_user')->token()->revoke();
    }

    /**
     * @OA\Get(
     *     path="/me",
     *     operationId="auth.me",
     *     summary="Retrieve authenticated user data",
     *     description="**Retrouver les données de l'utilisateur authentifié**  
               ❗ Action limitée au compte utilisateur authentifié par token
           ",
     *     tags={"Auth"},
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=401, description="Unauthenticated"),
     *     @OA\Response(response=403, description="Forbidden")
     * )
     *
     * ----
     * 
     * Return authenticated user
     * @param GetCurrentUserRequest $request - The request
     * @return User
     */
    public function getCurrentUser (GetCurrentUserRequest $request)
    {
        // Return authenticated user
        return app('auth_user');
    }

    /**
     * @OA\Patch(
     *     path="/me/password",
     *     operationId="password.update",
     *     summary="Update authenticated user password",
     *     description="**Modifier le mot de passe de l'utilisateur courant**  
               ❗ Action limitée au compte utilisateur authentifié par token
           ",
     *     tags={"Auth"},
     *     @OA\RequestBody(
     *         description="User password data to update",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Requests.Auth.UpdatePassword")
     *         )
     *     ),
     *     @OA\Response(response=200, description="Successful operation", @OA\MediaType(mediaType="application/json")),
     *     @OA\Response(response=401, description="Unauthenticated"),
     *     @OA\Response(response=403, description="Forbidden"),
     *     @OA\Response(response=404, description="Not found"),
     *     @OA\Response(response=422, description="Unprocessable entity")
     *  )
     * 
     * ----
     * 
     * Change user password
     * @param UpdatePasswordRequest $request - The request
     * @return Response
     */
    public function updatePassword (UpdatePasswordRequest $request)
    {
        // Validate the request
        $validated = $request->validated();
        // Password security
        $doesPasswordMatches = Hash::check($validated['current_password'], app('auth_user')->password);
        if (!$doesPasswordMatches) {
            return errorResponse(401, Error::find('e0022'));
        }
        // Password
        app('auth_user')->password = Hash::make($validated['password']);
        // Save and return modified user
        app('auth_user')->save();
        return response('Success', 200);
    }

    /**
     * @OA\Post(
     *     path="/reset-password",
     *     operationId="password.reset",
     *     summary="Reset user password",
     *     description="**Réinitialiser le mot de passe de l'utilisateur**",
     *     tags={"Auth"},
     *     @OA\RequestBody(
     *         description="Password data to reset",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Requests.Auth.ResetPassword")
     *         )
     *     ),
     *     @OA\Response(response=200, description="Successful operation", @OA\MediaType(mediaType="application/json")),
     *     @OA\Response(response=422, description="Unprocessable entity")
     *  )
     * 
     * ----
     * 
     * Reset user password
     * @param ResetPasswordRequest $request - The request
     * @return Response
     */
    public function resetPassword (ResetPasswordRequest $request)
    {
        // Validate the request
        $validated = $request->validated();
    
        // Reset password
        $status = Password::reset(
            $validated,
            function ($user, $password) use ($validated) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));
                // Save user
                $user->save();
                // Emit event
                event(new PasswordReset($user));
            }
        );
    
        // Return 200 if success, else 500 server error
        return $status == Password::PASSWORD_RESET
            ? response(__($status), 200)
            : response(__($status), 500);
    }

}
