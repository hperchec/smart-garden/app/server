<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

use App\Models\Error;
use App\Models\Circuit;

use App\Http\Requests\Circuit\IndexRequest;
use App\Http\Requests\Circuit\CreateRequest;
use App\Http\Requests\Circuit\RetrieveRequest;
use App\Http\Requests\Circuit\UpdateRequest;
use App\Http\Requests\Circuit\DeleteRequest;
use App\Http\Requests\Circuit\StatusRequest;
use App\Http\Requests\Circuit\TurnRequest;

class CircuitController extends Controller
{
    /**
     * Create a new controller instance
     * @codeCoverageIgnore
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @OA\Get(
     *     path="/circuits",
     *     operationId="circuits.index",
     *     summary="Retrieve all circuits",
     *     description="**Retrouver les circuits**  
               ❗ Authentification nécessaire
           ",
     *     tags={"Circuits"},
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=401, description="Unauthenticated"),
     *     @OA\Response(response=404, description="Not found")
     * )
     *
     * ----
     *
     * Return all circuits
     * @param IndexRequest $request - The request
     * @return Circuit[]|Response
     */
    public function index (IndexRequest $request)
    {
        return Circuit::with(['history'])->get();
    }

    /**
     * @OA\Post(
     *     path="/circuits/create",
     *     operationId="circuits.create",
     *     summary="Create circuit",
     *     description="**Créer un nouveau circuit**  
               ❗ Authentification nécessaire
           ",
     *     tags={"Circuits"},
     *     @OA\RequestBody(
     *         description="Circuit data to create",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Requests.Circuit.Create")
     *         )
     *     ),
     *     @OA\Response(response=200, description="Successful operation", @OA\MediaType(mediaType="application/json")),
     *     @OA\Response(response=422, description="Unprocessable entity")
     *  )
     * 
     * ----
     *
     * Create new circuit
     * @param CreateRequest $request - The request
     * @return Circuit|Response
     */
    public function create (CreateRequest $request)
    {
        // Validate the request
        $validated = $request->validated();
        // New circuit
        $circuit = new Circuit();
        // Channel
        $circuit->channel = $validated['channel'];
        // Name
        $circuit->name = $validated['name'];
        // Description
        $circuit->description = $validated['description'];
        // Color
        $circuit->color = $validated['color'];
        // Save
        $circuit->save();
        // Return created circuit
        return $circuit;
    }

    /**
     * @OA\Get(
     *     path="/circuits/{circuitId}",
     *     operationId="circuits.retrieve",
     *     summary="Retrieve a circuit",
     *     description="**Retrouver un circuit**  
               ❗ Authentification nécessaire
           ",
     *     tags={"Circuits"},
     *     @OA\Parameter(
     *         name="circuitId",
     *         in="path",
     *         description="Identifiant du circuit (ID)",
     *         required=true
     *     ),
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=401, description="Unauthenticated"),
     *     @OA\Response(response=404, description="Not found")
     * )
     *
     * ----
     * 
     * Return the found circuit by unique id
     * @param RetrieveRequest $request - The request
     * @param Circuit $circuit - The circuit
     * @return Circuit|Response
     */
    public function retrieve (RetrieveRequest $request, Circuit $circuit)
    {
        return $circuit->with(['history']);
    }

    /**
     * @OA\Patch(
     *     path="/circuits/{circuitId}",
     *     operationId="circuits.update",
     *     summary="Update circuit",
     *     description="**Modifier un circuit**  
               ❗ Authentification nécessaire
           ",
     *     tags={"Circuits"},
     *     @OA\Parameter(
     *         name="circuitId",
     *         in="path",
     *         description="Identifiant du circuit (ID)",
     *         required=true
     *     ),
     *     @OA\RequestBody(
     *         description="Circuit data to update",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Requests.Circuit.Update")
     *         )
     *     ),
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=401, description="Unauthenticated"),
     *     @OA\Response(response=403, description="Forbidden"),
     *     @OA\Response(response=404, description="Not found"),
     *     @OA\Response(response=422, description="Unprocessable entity")
     *  )
     * 
     * ----
     *
     * Update existing circuit
     * @param UpdateRequest $request - The request
     * @param Circuit $circuit - The circuit
     * @return Circuit|Response
     */
    public function update (UpdateRequest $request, Circuit $circuit)
    {
        // Validate the request
        $validated = $request->validated();
        // Assign each new property value
        foreach ($validated as $key => $value) {
            $circuit->$key = $value;
        }
        // Save and return modified circuit
        $circuit->save();
        return $circuit;
    }

    /**
     * @OA\Delete(
     *     path="/circuits/{circuitId}",
     *     operationId="circuits.delete",
     *     summary="Delete circuit",
     *     description="**Supprimer un circuit**  
               ❗ Authentification nécessaire
           ",
     *     tags={"Circuits"},
     *     @OA\Parameter(
     *         name="circuitId",
     *         in="path",
     *         description="Identifiant du circuit (ID)",
     *         required=true
     *     ),
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=401, description="Unauthenticated"),
     *     @OA\Response(response=403, description="Forbidden"),
     *     @OA\Response(response=404, description="Not found")
     * )
     *
     * ----
     * 
     * Delete existing circuit
     * @param DeleteRequest $request - The request
     * @param Circuit $circuit - The circuit
     * @return Response
     */
    public function delete (DeleteRequest $request, Circuit $circuit) {
        // Delete circuit
        $circuit->delete();
        return response('Circuit successfully deleted.', 200);
    }

    /**
     * @OA\Get(
     *     path="/circuits/{circuitId}/status",
     *     operationId="circuit.status",
     *     summary="Get circuit status",
     *     description="**Obtenir le statut d'un circuit**  
               ❗ Authentification nécessaire
           ",
     *     tags={"Circuits"},
     *     @OA\Parameter(
     *         name="circuitId",
     *         in="path",
     *         description="Identifiant du circuit (ID)",
     *         required=true
     *     ),
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=401, description="Unauthenticated"),
     *     @OA\Response(response=403, description="Forbidden"),
     *     @OA\Response(response=404, description="Not found")
     * )
     *
     * ----
     * 
     * Get status of existing circuit
     * @param StatusRequest $request - The request
     * @param Circuit $circuit - The circuit
     * @return Response
     */
    public function status (StatusRequest $request, Circuit $circuit) {
        // Get channel status by calling bash command
        $process = new Process(["smart-garden", "-v", "NONE", "status", $circuit->channel]);
        $process->run();
        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        // Get output
        $output = $process->getOutput();
        // Remove 'new line' character at end
        $output = trim(preg_replace('/\s\s+/', '', $output));
        // Return status
        return response([
            'status' => $output == 'ON' ? true : false
        ], 200);
    }

    /**
     * @OA\Post(
     *     path="/circuits/{circuitId}/turn",
     *     operationId="circuit.turn",
     *     summary="Turn a circuit on/off",
     *     description="**Allumer ou éteindre un circuit**  
               ❗ Authentification nécessaire
           ",
     *     tags={"Circuits"},
     *     @OA\Parameter(
     *         name="circuitId",
     *         in="path",
     *         description="Identifiant du circuit (ID)",
     *         required=true
     *     ),
     *     @OA\RequestBody(
     *         description="Circuit data to update",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(ref="#/components/schemas/Requests.Circuit.Turn")
     *         )
     *     ),
     *     @OA\Response(response=200, description="Successful operation"),
     *     @OA\Response(response=401, description="Unauthenticated"),
     *     @OA\Response(response=403, description="Forbidden"),
     *     @OA\Response(response=404, description="Not found")
     * )
     *
     * ----
     * 
     * Turn a circuit on/off
     * @param TurnRequest $request - The request
     * @param Circuit $circuit - The circuit
     * @return Response
     */
    public function turn (TurnRequest $request, Circuit $circuit) {
        // Validate the request
        $validated = $request->validated();
        // Turn on/off the target circuit
        $process = new Process(["smart-garden", "turn", $circuit->channel, $validated['status'] ? 'on' : 'off']);
        $process->run();
        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        // Get output
        $output = $process->getOutput();
        // Insert new item in history
        $circuit->setHistory($validated['status']);
        // Return status
        return response([
            'status' => $validated['status']
        ], 200);
    }

}
