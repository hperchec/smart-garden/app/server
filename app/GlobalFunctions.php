<?php

use Illuminate\Support\Str;

use App\Models\Error;

/**
 * Return customized error response
 * @param int $httpStatus - The HTTP status code
 * @param Error - $error
 * @param Array $payload - (optional) The error payload (default: [])
 * @return Illuminate\Http\Response
 */
function errorResponse (int $httpStatus, Error $error, Array $payload = [])
{
    return response([
        getErrorResponseKeyName() => [
            'errorCode' => $error->error_id,
            'errorMessage' => [
                'en' => $error->en_message,
                'fr' => $error->fr_message
            ],
            'payload' => $payload
        ]
    ], $httpStatus);
}

/**
 * getErrorResponseKeyName
 * @return string
 */
function getErrorResponseKeyName () {
    return Str::snake(env('APP_NAME')) . '_error_response';
}

/**
 * isAuthenticatedUser
 * @param int $userId - The user id to compare with authenticated user id
 * @return bool
 */
function isAuthenticatedUser (int $userId) {
    return app('auth_user')->id === $userId;
}
