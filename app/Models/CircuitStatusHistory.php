<?php

namespace App\Models;

class CircuitStatusHistory extends BaseModel
{

    /**
     * OVERWRITTEN ELOQUENT MODEL PROPERTIES
     */

    /**
     * Table name
     * @var string
     */
    protected $table = "circuit_status_history";

    /**
     * The attributes that are mass assignable
     * @var array
     */
    protected $fillable = [
        'status'
    ];

    /**
     * If model uses 'created_at' and 'updated_at' fields
     * @var array
     */
    public $timestamps = true;

    const UPDATED_AT = null; // Disable updated_at field

    /**
     * The attributes that should be cast to native types
     * @var array
     */
    protected $casts = [
        // ...
    ];

    /**
     * CUSTOM PROPERTIES
     */

    /**
     * List of relations
     * @var array
     */
    protected $relationNames = [
        //
    ];

     /**
     * Attribute / relation names to change when return JSON response (via toArray())
     * @var array
     */
    protected $attributeNamesMap = [
        //
    ];

    /**
     * RELATIONS
     */

    // ...

    /**
     * SCOPES
     */

    // ...

    /**
     * CUSTOM METHODS
     */

    // ...

    /**
     * OVERWRITTEN BASE MODEL METHODS
     */

    /**
     * Define data visibility depending of user rights
     * @param Array $data - The original data
     * @return array
     */
    public static function dataVisibility ($data) {
        // Create base array with all data attributes to 'true'
        $visibility = array_fill_keys(array_keys($data), true);
        // Do logic here
        return $visibility;
    }

    /**
     * OVERWRITTEN ELOQUENT MODEL METHODS
     */

    /**
     * Overwrite toArray() method
     * @return array
     */
    public function toArray()
    {
        // Attributes
        $attributes = $this->attributesToArray();
        // Get related resources array
        $related = $this->relationsToArray();
        // Full data
        $data = array_merge($attributes, $related);
        // Do logic here...
        // Return formatted & cleaned object
        return static::cleanData($data);
    }

}
