<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;

use App\Http\Traits\BaseModelTrait;
use App\Http\Traits\ExcludableTrait;

abstract class BaseModel extends Model
{
    use BaseModelTrait, ExcludableTrait, HasRelationships;

    /**
     * Create a new Eloquent model instance.
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
    }

    /**
     * PROPERTIES
     */

    /**
     * List of relations
     * @var array
     */
    protected $relationNames = []; // Empty by default

    /**
     * Attribute / relation names to change when return JSON response (via toArray())
     * @var array
     */
    protected $attributeNamesMap = []; // Empty by default

}
