<?php

namespace App\Models;

class Error extends BaseModel
{

    /**
     * OVERWRITTEN ELOQUENT MODEL PROPERTIES
     */

    /**
     * The primary key column name
     * @var string
     */
    protected $primaryKey = 'error_id';

    /**
     * The type of primary key
     * @var string
     */
    protected $keyType = 'string';

    /**
     * If model uses 'created_at' and 'updated_at' fields
     * @var array
     */
    public $timestamps = false;

    /**
     * CUSTOM PROPERTIES
     */

    /**
     * List of relations
     * @var array
     */
    protected $relationNames = [
        //
    ];

     /**
     * Attribute / relation names to change when return JSON response (via toArray())
     * @var array
     */
    protected $attributeNamesMap = [
        //
    ];

    /**
     * RELATIONS
     */

    // ...

    /**
     * SCOPES
     */

    // ...

    /**
     * CUSTOM METHODS
     */

    /**
     * Replace occurences in translations messages and return the Error
     * @param array $replace - The replacements
     * @return Error
     */
    public function fillText (array $replace) 
    {
        // Prepare translated messages
        $replace_fr = [];
        $replace_en = [];
        // Sort messages
        foreach ($replace as $key => $value) {
            $replace_fr[] = $replace[$key]['fr'];
            $replace_en[] = $replace[$key]['en'];
        }
        // Transform parameters to find
        $search = array_map(function ($key) {
            return '{' . $key . '}';
        }, array_keys($replace));
        // Replace
        $this->fr_message = str_replace($search, $replace_fr, $this->fr_message);
        $this->en_message = str_replace($search, $replace_en, $this->en_message);
        // Return Error
        return $this;
    }

    /**
     * OVERWRITTEN BASE MODEL METHODS
     */

    /**
     * Define data visibility depending of user rights
     * @param Array $data - The original data
     * @return array
     */
    public static function dataVisibility ($data) {
        // Create base array with all data attributes to 'true'
        $visibility = array_fill_keys(array_keys($data), true);
        // Do logic here
        return $visibility;
    }

    /**
     * OVERWRITTEN ELOQUENT MODEL METHODS
     */

    /**
     * Overwrite toArray() method
     * @return array
     */
    public function toArray()
    {
        // Attributes
        $attributes = $this->attributesToArray();
        // Get related resources array
        $related = $this->relationsToArray();
        // Full data
        $data = array_merge($attributes, $related);
        // Do logic here...
        // Return formatted & cleaned object
        return static::cleanData($data);
    }

}
