<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\Models\User;
use App\Models\Error;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the current user can update data
     * @param  \App\Models\User  $me - The authenticated user
     * @param  \App\Models\User  $user - The target user
     * @return mixed
     */
    public function update (User $me, User $user)
    {
        // Authenticated user can only update its OWN data
        if ($me->id !== $user->id) {
            return $this->deny(Error::find('e0012'));
        }
        // Everything is ok...
        return true;
    }

}
