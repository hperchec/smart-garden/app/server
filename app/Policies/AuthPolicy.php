<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\Models\User;
use App\Models\Error;

class AuthPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can log out
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function logout (User $user)
    {
        // Everything is ok...
        return true;
    }

    /**
     * Determine whether the current user can get data
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function getCurrentUser (User $user)
    {
        // Everything is ok...
        return true;
    }

    /**
     * Determine whether the current user can update password
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function updatePassword (User $user)
    {
        // Everything is ok...
        return true;
    }

}
