<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Symfony\Component\HttpFoundation\ParameterBag;

use App\Models\User;
use App\Models\Circuit;

class CircuitPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view all models
     * @param User $user
     * @return mixed
     */
    public function index (User $user)
    {
        // Everything is ok
        return true;
    }

    /**
     * Determine whether the user can create a model
     * @param User $user
     * @return mixed
     */
    public function create (User $user)
    {
        // Everything is ok
        return true;
    }

    /**
     * Determine whether the user can view the model
     * @param User $user
     * @param Circuit $circuit
     * @return mixed
     */
    public function retrieve (User $user, Circuit $circuit)
    {
        // Everything is ok
        return true;
    }

    /**
     * Determine whether the user can update the model
     * @param User $user
     * @param Circuit $circuit
     * @return mixed
     */
    public function update (User $user, Circuit $circuit)
    {
        // Everything is ok
        return true;
    }

    /**
     * Determine whether the user can delete the model
     * @param User $user
     * @param Circuit $circuit
     * @return mixed
     */
    public function delete (User $user, Circuit $circuit)
    {
        // Everything is ok
        return true;
    }

    /**
     * Determine whether the user can get status of the circuit
     * @param User $user
     * @param Circuit $circuit
     * @return mixed
     */
    public function status (User $user, Circuit $circuit)
    {
        // Everything is ok
        return true;
    }

    /**
     * Determine whether the user can turn the circuit on/off
     * @param User $user
     * @param Circuit $circuit
     * @return mixed
     */
    public function turn (User $user, Circuit $circuit)
    {
        // Everything is ok
        return true;
    }

}
